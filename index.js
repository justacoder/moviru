const shell = require('shelljs');
const testJson = require('/mnt/d/torrent/metadata.json');
const acronyms = require('./acronyms.json');
const tmdb = require('./tmdb');
const FSS = require('./video-folder-manager.js');
const filters = {
  acronyms: /MPEG-4|mpeg|Multi|Multi5|NTSC|PAL|AC3|MP3|LD|DD5|5.1|dimension|MD|CAMRip|CAM|WP|WORKPRINT|PPV|PPVRip|DDC|DVDRip|DSR|DSRip|SATRip|DTHRip|DVBRip|HDTV|PDTV|TVRip|HDTVRip|WEB|DL|DD5|WEBDL|WEB DL|WEB-DL|HDRip|WEB-Cap|WEBCAP|WEB Cap|BRRip|BDRip|DVDRip|DVDSCR|HDTVRip|TVRip|TC|PPVRip|R5|VHSSCR|TS|CAM|((x|X)264)|((x|X)264)|((x|X)-264)|((x|X).264)|((x|X).265)|265|264|BluRay|xvid|aac|YIFY|HDDVD|HD|DVD/gi,
  delimeter: /(\.|\s|-|\+|_)/g,
  brackets: /\[[a-zA-Z0-9.\-_]*\]|\([a-zA-Z0-9.\-_]*\)/g,
  pixelQuality: /(\[\d{3,4}(p|P)\])|(\d{3,4}(p|P))|(\d{3,4}x\d{3,4})/g,
  year: /(\[[1-2][0-9][0-9][0-9]\])|(((19[0-9])|(20[0-1]))[0-9])/g,
  episode: /s\d{1,2}e\d{1,2}/gi,
};

// const nodes = FSS.processAll('/mnt/d/torrent').then(files => {
//   console.log(files);
//   console.log(FSS.nodes);
//   return FSS.writeMetadata('/mnt/d/torrent');
// });

/*shell.exec('mkvmerge -I -J content/episode_1.mkv', function(*/
//code,
//stdout,
//stderr
//) {
//console.log('Exit code:', code);

//if (code === 0) {
//const str = stdout.toString();
//const vidProps = JSON.parse(str);
//debugger;
//console.log('Program output:', str);
//} else {
//console.log('Program stderr:', stderr);
//}
/*});*/

const result = Object.keys(testJson)
  .map(path => path.split('/').slice(-1)[0])
  .map(item => {
    return item
      .replace(filters.year, '')
      .replace(filters.pixelQuality, '')
      .replace(filters.acronyms, '')
      .replace(filters.brackets, '')
      .replace(filters.delimeter, ' ')
      .split(/\s+/)
      .filter(i => i)
      .slice(0, -1)
      .reduce(
        (prev, next) =>
          prev.concat(
            next
              .replace(filters.year, '')
              .replace(filters.pixelQuality, '')
              .replace(filters.acronyms, '')
              .replace(filters.brackets, '')
              .replace(filters.delimeter, '')
          ),
        []
      )
      .filter(i => i)
      .map(s =>
        s
          .replace(filters.year, '')
          .replace(filters.pixelQuality, '')
          .replace(filters.acronyms, '')
          .replace(filters.brackets, '')
          .replace(filters.delimeter, '')
      );
  })
  .filter(item => item)
  .map(words => words.join(' '));

let tv = [];
let movie = [];

result.forEach(str => {
  if (filters.episode.test(str)) {
    tv.push(str);
  } else {
    movie.push(str);
  }
});

tv.forEach(str => {
  tmdb.searchTV(str).then(result => {
    console.log('TV');
    console.log(JSON.stringify(result, null, 2));
  });
});

movie.forEach(str => {
  tmdb.searchAll(str).then(result => {
    console.log('ALL');
    console.log(JSON.stringify(result, null, 2));
  });
});
