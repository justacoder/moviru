const Storage = require('@google-cloud/storage');
const { cloudStorage } = require('./config.js');
const { projectId, bucket } = cloudStorage;

class CloudStorage {
  static listAll() {
    return this.storage
      .bucket(this.bucket)
      .getFiles()
      .then(results => results[0].map(file => file.name))
      .catch(err => {
        console.error('ERROR:', err);
      });
  }

  static listFolder(prefix) {
    return this.storage
      .bucket(this.bucket)
      .getFiles({ prefix })
      .then(results => results[0].map(file => file.name))
      .catch(err => {
        console.error('ERROR:', err);
      });
  }

  static upload(localPath, cloudPath) {}

  static download(cloudPath, localPath) {}
}

CloudStorage.bucket = bucket;
CloudStorage.project = projectId;
CloudStorage.storage = new Storage({
  projectId: projectId,
});

module.exports = CloudStorage;
