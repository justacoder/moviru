module.exports.chain = function(promises) {
  return promises.reduce((prev, next) => prev.then(next), Promise.resolve());
};

module.exports.getRandomInt = function(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};
