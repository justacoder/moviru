const shell = require('shelljs');
const logUpdate = require('log-update');
const cliSpinners = require('cli-spinners');
const fs = require('fs');
const hidefile = require('hidefile');
const videoExtensions = require('./video-extensions.js');
const audioExtensions = require('./audio-extensions.js');
const subtitleExtensions = require('./subtitle-extensions.js');
const { chain, getRandomInt } = require('./utils.js');

class FSNode {
  constructor(path) {
    this.path = path;
    this.stats = {};
    this.info = {};
  }
}

class VideoFolderManager {
  static generateNode(path) {
    return this.getMediaInfo(path)
      .then(info => {
        const node = new FSNode(path);
        node.stats = fs.lstatSync(path);
        node.info = info;
        this.nodes[path] = node;
        console.log(`NODE CREATED => ${JSON.stringify(node, null, 4)}`);
        return node;
      })
      .catch(e => `FFMPEG ERROR => ${e}`);
  }

  static isFileAllowed(path) {
    return (
      !this.isHidden(path) &&
      this.allowedFileTypes.includes(path.substring(path.lastIndexOf('.') + 1))
    );
  }

  static exists(path) {
    try {
      return !!fs.lstatSync(filePath);
    } catch (err) {
      return false;
    }
  }

  static isHidden(path) {
    return hidefile.isHiddenSync(path);
  }

  static isFolder(path) {
    return fs.lstatSync(path).isDirectory();
  }

  static processAll(path) {
    return Promise.all(this.processFolder(path)).catch(e => console.log(e));
  }

  static processFolder(path) {
    const all = fs.readdirSync(path).map(p => `${path}/${p}`);
    const folders = all.filter(n => this.isFolder(n));
    const files = all.filter(n => !this.isFolder(n));
    return folders.reduce(
      (prev, next) => [...prev, ...this.processFolder(next)],
      all.filter(n => !this.isFolder(n)).map(n => this.processFile(n))
    );
  }

  static writeMetadata(root) {
    fs.writeFileSync(
      `${root}/metadata.json`,
      JSON.stringify(this.nodes, null, 4)
    );
  }

  static processFile(path) {
    if (this.isFileAllowed(path)) {
      return this.generateNode(path);
    } else {
      fs.unlinkSync(path);
      return Promise.reject(null);
    }
  }

  static getNode(path) {
    return this.nodes[path];
  }

  static shouldBeConverted(path) {
    return (
      this.nodes[path].info.format_name !== 'mov,mp4,m4a,3gp,3g2,mj2' &&
      !this.exists(this.withExtension(path, 'mp4'))
    );
  }

  static withExtension(path, extentsion) {
    return path.substring(0, path.lastIndexOf('.') + 1) + extension;
  }

  static convertAll(path) {
    this.processFolder(path).then(metadata => {
      const files = Object.keys(metadata);
      const convertibles = files.filter(this.shouldBeConverted.bind(this));
      const promises = convertibles.map(path => this.convert.bind(this, path));

      return chain(promises);
    });
  }

  static convert(path) {
    let i = 0;
    const spinners = Object.keys(cliSpinners);
    const spinner = cliSpinners[spinners[getRandomInt(0, spinners.length)]];

    return new Promise((resolve, reject) => {
      FfmpegCommand(path)
        .format('mp4')
        .videoCodec('libx264')
        .audioCodec('aac')
        .outputOptions(['-preset fast', '-movflags +faststart', '-crf 17'])
        .on('start', function(commandLine) {
          const frames = spinner.frames;
          logUpdate(frames[(i = ++i % frames.length)], `Encoding ${path} 0 %`);
        })
        .on('progress', function(info) {
          const frames = spinner.frames;
          logUpdate(
            frames[(i = ++i % frames.length)],
            `Encoding ${path} ${info.percent.toFixed(2)} %`
          );
        })
        .on('end', function() {
          console.log(`Encoded all successfully`);
          resolve(true);
        })
        .on('error', function(err) {
          console.log('an error happened: ' + err.message);
          reject(err);
        })
        .save(this.withExtension(path, 'mp4'));
    });
  }

  static getMediaInfo(path) {
    return new Promise((resolve, reject) => {
      shell.exec(
        `ffprobe -print_format json -show_format -show_streams "${path}"`,
        function(code, stdout, stderr) {
          if (code === 0) {
            const str = stdout.toString();
            const mediaInfo = JSON.parse(str);

            resolve(mediaInfo);
          } else {
            reject(stderr);
          }
        }
      );
    });
  }
}

VideoFolderManager.allowedFileTypes = [
  ...videoExtensions,
  ...audioExtensions,
  ...subtitleExtensions,
];

VideoFolderManager.nodes = {};

module.exports = VideoFolderManager;
