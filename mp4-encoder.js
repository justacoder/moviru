const extensions = require('./video-extensions.js');
const logUpdate = require('log-update');
const cliSpinners = require('cli-spinners');
const fs = require('fs');
const FfmpegCommand = require('fluent-ffmpeg');
const winattr = require('winattr');
const converter = new FfmpegCommand();
const ffmpeg = new FfmpegCommand();

module.exports = class Encoder {
  static chain (promises) {
    return promises.reduce((prev, next) => prev.then(next), Promise.resolve());
  }

  static isFolder (path) {
    const result = fs.lstatSync(path);
    return result.isDirectory();
  }

  static parseDirectory (path) {
    const all = fs.readdirSync(path).map((p) => `${path}/${p}`);

    const folders = all.filter((torrent) => {
      return this.isFolder(torrent);
    });

    const files = folders.reduce(
      (prev, next) => [
        ...prev,
        ...this.parseDirectory(next)
      ],
      all.filter((torrent) => {
        return !this.isFolder(torrent);
      })
    );

    files.forEach((file) => {
      if (winattr.getSync(file).hidden) {
        fs.unlinkSync(file);
      }
    });

    return files.filter((file) => {
      return extensions.includes(file.substring(file.lastIndexOf('.') + 1));
    });
  }

  static probe (path) {
    return new Promise((resolve, reject) => {
      FfmpegCommand.ffprobe(path, (err, metadata) => {
        if (err) reject(err);
        else {
          this.metadata[path] = metadata.format;
          resolve(metadata.format);
        }
      });
    });
  }

  static generateJSONDescriptor (path) {
    const files = this.parseDirectory(path);
    const promises = [];
    let allMetadata = {};

    console.log(`Found ${files.length} files`);

    files.forEach((f) => {
      promises.push(this.probe.bind(this, f));
    });

    return this.chain(promises)
      .then((results) => {
        fs.writeFileSync(`${path}/formats.json`, JSON.stringify(this.metadata, null, 2));

        return this.metadata;
      })
      .catch((e) => {
        console.log(e);
        throw e;
      });
  }

  static getRandomInt (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  static convert (path) {
    let i = 0;
    const spinners = Object.keys(cliSpinners);
    const spinner = cliSpinners[spinners[this.getRandomInt(0, spinners.length)]];

    return new Promise((resolve, reject) => {
      FfmpegCommand(path)
        .format('mp4')
        .videoCodec('libx264')
        .audioCodec('aac')
        .outputOptions([
          '-preset fast',
          '-movflags +faststart',
          '-crf 17'
        ])
        .on('start', function (commandLine) {
          const frames = spinner.frames;
          logUpdate(frames[(i = ++i % frames.length)], `Encoding ${path} 0 %`);
        })
        .on('progress', function (info) {
          const frames = spinner.frames;
          logUpdate(frames[(i = ++i % frames.length)], `Encoding ${path} ${info.percent.toFixed(2)} %`);
        })
        .on('end', function () {
          console.log(`Encoded all successfully`);
          resolve(true);
        })
        .on('error', function (err) {
          console.log('an error happened: ' + err.message);
          reject(err);
        })
        .save(`${path.substring(0, path.lastIndexOf('.'))}.mp4`);
    });
  }

  static bulkConvert (path) {
    return this.convert(path).then((status) => {
      const newPath = path.substring(0, path.lastIndexOf('.')) + '.mp4';

      fs.unlinkSync(path);
      delete this.metadata[path];
      return this.probe(newPath);
    });
  }

  static shouldBeConverted (path) {
    return this.metadata[path].format_name !== 'mov,mp4,m4a,3gp,3g2,mj2';
  }

  static convertAll (path) {
    this.generateJSONDescriptor(path).then((metadata) => {
      const files = Object.keys(metadata);
      const convertibles = files.filter(this.shouldBeConverted.bind(this));
      const promises = convertibles.map((path) => this.bulkConvert.bind(this, path));

      return this.chain(promises).then(this.generateJSONDescriptor.bind(this, path));
    });
  }

  static listFiles () {
    console.log(Object.keys(this.metadata).map((path) => path.substring(path.lastIndexOf('/') + 1)));
  }
};

Encoder.metadata = {};
