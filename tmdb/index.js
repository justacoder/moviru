const http = require('https');
const config = require('../config');

module.exports = class TMDB {
  static search (str, type) {
    const s = encodeURIComponent(str);
    const path = `/3/search/${type}?include_adult=false&page=1&query=${s}&language=en-US&api_key=${config.API_KEY}`;
    const options = {
      method: 'GET',
      hostname: 'api.themoviedb.org',
      port: null,
      path,
      headers: {}
    };

    return new Promise((resolve, reject) => {
      const req = http.request(options, function (res) {
        const chunks = [];

        res.on('data', function (chunk) {
          chunks.push(chunk);
        });

        res.on('end', function () {
          const result = Buffer.concat(chunks);
          resolve(JSON.parse(result.toString()));
        });

        res.on('error', function (err) {
          reject(err);
        });
      });

      req.write('{}');
      req.end();
    });
  }

  static searchPerson (str) {
    return this.search(str, 'person');
  }

  static searchMovie (str) {
    return this.search(str, 'movie');
  }

  static searchTV (str) {
    return this.search(str, 'tv');
  }

  static searchAll (str) {
    return this.search(str, 'multi');
  }
};
