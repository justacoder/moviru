const gulp = require('gulp');
const { exec } = require('child_process');

gulp.task('default', cb =>
  exec('yarn start', (err, stdout, stderr) => {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  })
);

gulp.watch('./**/*.*', ['default']);
