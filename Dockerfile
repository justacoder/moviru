FROM ubuntu:xenial
RUN mkdir /app
RUN mkdir /app/content
WORKDIR /app
COPY ./content /app/content/
RUN apt-get update -y
RUN apt-get install git make pkg-config g++ zlib1g-dev firefox-dev libfreetype6-dev libjpeg62-dev libpng12-dev libopenjpeg-dev libmad0-dev libfaad-dev libogg-dev libvorbis-dev libtheora-dev liba52-0.7.4-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libavresample-dev libxv-dev x11proto-video-dev libgl1-mesa-dev x11proto-gl-dev linux-sound-base libxvidcore-dev libssl-dev libjack-dev libasound2-dev libpulse-dev libsdl1.2-dev dvb-apps libavcodec-ffmpeg-extra56 libavdevice-dev libmozjs185-dev nasm yasm mkvtoolnix -y
RUN git clone git://git.videolan.org/x264.git x264 && cd x264 && ./configure --disable-asm --enable-shared --disable-cli --enable-strip --disable-avs --disable-swscale --disable-lavf --disable-ffms --disable-gpac --disable-opencl && make -j && make install && cd ../
RUN git clone https://github.com/gpac/gpac.git gpac && cd gpac && ./configure && make && make install && make install-lib && cd ../
CMD mkvmerge -I ./content/movie.mkv
